/*
 * (511,304,25) BCH Decoder
 *
 * Copyright 2015: Distributed under 2-clause BSD license as contained in COPYING file.
 */

//	Compute LFSR term module:
//	Compute LFSR terms based on given polynomials
module	compute_lfsr_term #(
	parameter	ILENGTH = 1,		//	Length of input sequence
	parameter	OLENGTH = 1,		//	Length of output sequence
	parameter	[ILENGTH*OLENGTH-1:0]	POLY_CON = 1	// Concetenated polynomial (low-degree polynomial first)
)(
	input	[ILENGTH-1:0]	in,		// 	Input sequence (with high-degree terms first)
	output	[OLENGTH-1:0]	out		//	Output sequence (with high-degree terms first)
);
	
	wire	[ILENGTH*OLENGTH-1:0]	terms;

	generate
		genvar i, j;
		//	Obtain term polynomial and compute all terms
		for (i=0; i<ILENGTH; i=i+1) begin : COMPUTE
			//	Obtain term polynomial
			localparam	[OLENGTH-1:0]	POLY = POLY_CON[(ILENGTH-i-1)*OLENGTH+:OLENGTH];

			//	Compute terms
			assign terms[i*OLENGTH+:OLENGTH] = in[i]?POLY:'b0;
		end

		//	XOR all terms to obtain an output
		for (i=0; i<OLENGTH; i=i+1) begin : XOR
			wire	[ILENGTH-1:0]	temp;
			for (j=0; j<ILENGTH; j=j+1) begin : STORE
				//	Store terms value at position i to temp
				assign temp[j] = terms[j*OLENGTH+i];
			end
			assign out[i] = ^temp;
		end
	endgenerate

endmodule

//	Counter module:
//	Initial value is set to 0, Increment if not reset and enable is high
module counter #(
	parameter	LENGTH = 1
)(
	input	clk,
	input	enb,
	input	rst,
	output	reg	[LENGTH-1:0]	cntr = 0
);
	always @(posedge clk) begin
		if (rst)
			cntr <= #1 1'b0;
		else if (enb)
			cntr <= #1 cntr + 1'b1;
	end

endmodule

//	Finite adder
module	finite_parallel_adder #(
	parameter LENGTH = 9,
	parameter NUM_INPUT = 1
)(
	input	[LENGTH*NUM_INPUT-1:0]	in,
	output	[LENGTH-1:0]	out
);
	genvar i, j;

	for (i=0; i<LENGTH; i=i+1) begin : XOR
		wire [NUM_INPUT-1:0] z;
		for (j=0; j<NUM_INPUT; j=j+1) begin : STORE
			assign z[j] = in[j*LENGTH+i];
		end
		assign out[i] = ^z;
	end

endmodule

// Bit-parallel standard basis multiplier (PPBML)
module finite_parallel_multiplier #(
	parameter LENGTH = 9,
	parameter NUM_INPUT = 1
) (
	input 	[LENGTH-1:0] in1,
	input 	[LENGTH*NUM_INPUT-1:0] in2,
	output 	[LENGTH*NUM_INPUT-1:0] out
);
	wire	[LENGTH*LENGTH-1:0]	gen_matrix;

	gen_matrix #(.LENGTH(LENGTH)) p_mul_genmatrix (
		.in(in1),
		.out(gen_matrix)
	);

	matrixT_vector_multiply #(.ROW(LENGTH)) p_mul_Tmul [NUM_INPUT-1:0] (
		.matrix(gen_matrix),
		.vector(in2),
		.out(out)
	);

endmodule

// Create generate matrix
module gen_matrix #(
	parameter	LENGTH = 9,
	parameter	[LENGTH-1:0]	PRIM_POLY = 'b000010001
)(
	input	[LENGTH-1:0]	in,
	output	[LENGTH*LENGTH-1:0]	out
);
	genvar i;
	for (i=0; i<LENGTH; i=i+1) begin : GEN
		assign out[i*LENGTH+:LENGTH] = i? 
				((out[(i-1)*LENGTH+:LENGTH]<<1)^(((out[(i-1)*LENGTH+:LENGTH]>>(LENGTH-1))&1)?PRIM_POLY:0)):
				in;
	end

endmodule

// Multiplication between transpose of matrix and vector
module matrixT_vector_multiply #(
	parameter ROW = 4,
	parameter COLUMN = ROW
) (
	input 	[ROW*COLUMN-1:0]	matrix,
	input 	[ROW-1:0]	vector,
	output 	[COLUMN-1:0]	out
);
	wire	[COLUMN*ROW-1:0]	matrixT;

	transpose #(.ROW(ROW), .COLUMN(COLUMN)) mTv_T (
		.in(matrix),
		.out(matrixT)
	);

	matrix_vector_multiply #(.ROW(ROW), .COLUMN(COLUMN)) mTv_mul ( 
		.matrix(matrixT),
		.vector(vector),
		.out(out)
	);

endmodule

module transpose #(
	parameter ROW = 4,
	parameter COLUMN = 4
)(
	input	[ROW*COLUMN-1:0]	in,
	output	[COLUMN*ROW-1:0]	out
);
	genvar i,j;
	for (i=0; i<ROW; i=i+1) begin : FOREACHROW
		for (j=0; j<COLUMN; j=j+1) begin : TRANSPOSE
			assign out[j*COLUMN+i] = in[i*COLUMN+j];
		end
	end

endmodule

//	Multiplication between matrix and vector
module matrix_vector_multiply #(
	parameter ROW = 4,
	parameter COLUMN = ROW
)(
	input 	[COLUMN*ROW-1:0]	matrix,
	input 	[COLUMN-1:0]	vector,
	output 	[ROW-1:0]	out
);
	genvar i;
	for (i=0; i<ROW; i=i+1) begin : mult
		assign out[i] = ^(matrix[i*COLUMN+:COLUMN] & vector);
	end

endmodule
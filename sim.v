/*
 * (511,304,25) BCH Decoder
 *
 * Copyright 2015: Distributed under 2-clause BSD license as contained in COPYING file.
 */

module sim #(	
	parameter	M = 9,		//	Degree of field
	parameter	N = 511,	//	Code length
	parameter	K = 304,	//	Data length
	parameter	T = 25,		//	Number of correctable bits
	parameter	P = 207		//	Number of parity check bits
)(
	input			clk,
	input			rst,
	input			encode_start,
	input	[K-1:0]	data_in,	// d(X) = d_{k-1}X^{k-1} + d_{k-2}X^{k-2} + ... + d_{0}X^{0}
	input	[N-1:0]	error_in,	// e(X) = e_{n-1}X^{n-1} + e_{n-2}X^{n-2} + ... + e_{0}X^{0}
	input	[31:0]	num_err,
	output			ready
);

	//	----------------------------------------------------------
	//		Wires and Regs
	//	----------------------------------------------------------

//	(1)
	reg		[K-1:0]	encode_buffer;		//	Encoder buffer
	wire			encode_is_first;	//	Asserted if current output is the first output bit
	wire			encode_is_last;		//	Asserted if current output is the last output bit
	wire			encode_enable;		//	Encoder enable
	wire			encode_accept_new;	//	Asserted in case encoder accept new input 
	wire			encode_ready;
	wire	[15:0]	encode_in;			//	Encoder input
	wire	[15:0]	encode_out;			//	Encoder output

//	(2)
	reg		[N-1:0]	error_buffer;		//	Error buffer
	wire			syndrome_start;		//	Syndrome start
	wire			syndrome_enable;	//	Syndrome enable
	wire			syndrome_ready;
	wire			syndrome_finish;	
	wire	[15:0]	decode_in;			//	Decoder input or syndrome input
	wire	[(2*T+1)*M-1:M]	expanded_syndromes;

//	(3)
	wire			key_ready;
	wire			key_finish;
	wire	[(T+1)*M-1:0]	sigma;		//	Error location polynomial
	wire 	[5:0]	elp_degree;			//	Degree of error location polynomial

//	(4)
	wire			chien_ready;
	wire			chien_finish;
	wire			chien_is_first;		//	Asserted if error location is the first
	wire			chien_is_last;		//	Asserted if error location is the last
	wire	[15:0]	chien_error;		//	Error location for correcting received bits
	wire			chien_valid;
	wire			chien_fail;			//	Asserted if error is detected, but not correctable

//	(X)
	reg		[K-1:0]	data_at_syndrome;
	reg		[N-1:0]	error_at_syndrome;
	reg		[31:0]	num_err_at_syndrome;
	reg		[K-1:0]	data_at_key;
	reg		[N-1:0]	error_at_key;
	reg		[31:0]	num_err_at_key;
	reg		[K-1:0]	data_at_chien;
	reg		[N-1:0]	error_at_chien;
	reg		[31:0]	num_err_at_chien;
	reg		[N-1:0]	chien_error_buffer;	//	Error buffer for storing error location
	wire	[8:0]	chien_cntr;
	reg		[4:0]	temp1;				//	For simulating only
	wire	[N-1:0] temp2;

	//	----------------------------------------------------------
	//		Wire assignment
	//	----------------------------------------------------------

//	(0)
	assign ready = encode_ready && encode_enable;	//	Ready when encode is ready and enable
	
//	(1)
	// Don't assert start until we get th ready signal from syndrome
	assign encode_enable = (!encode_is_first||syndrome_ready)&&syndrome_enable;
	//	Keep feeding data until decoder is busy
	assign encode_accept_new = encode_start && encode_ready && encode_enable;
	//	Encoder input is load from data_in if new input is encoded. Otherwise, load data from buffer
	assign encode_in = encode_accept_new ? data_in[K-1:K-16] : encode_buffer[K-1:K-16];

//	(2)
	//	Don't assert start until syndrome is ready
	assign syndrome_start = (encode_is_first&&encode_enable)&&syndrome_ready;
	//	Add new data as long as key is not busy
	assign syndrome_enable = !syndrome_finish||key_ready;
	// assign syndrome_enable = !syndrome_finish||1'b1;
	//	Corrupt transmission. Error input is load from error_in if new data is encoded. Otherwise, load error from buffer
	assign decode_in = encode_accept_new ? (encode_out^error_in[N-1:N-16]) : (encode_out^error_buffer[N-1:N-16]);

//	(X)
	assign temp2 = (chien_is_last?chien_error[15:1]:(chien_error[15:0]<<(temp1*16-1)));

	//	----------------------------------------------------------
	//		Module declaration
	//	----------------------------------------------------------

//	(1)
	//	Encoding module
	bch_encode #(M, N, K, T, P) sim_encode (
		.clk(clk),
		.enb(encode_enable),
		.start(encode_start&&encode_ready),
		.data_in(encode_in),		//	Paralle inputs
		.data_out(encode_out),		//	Paralle outputs
		.is_first(encode_is_first),	//	Is a current cycle the first ouput cycle?
		.is_databit(),				//	Is a current cycle data bits?
		.is_paritybit(),			//	Is a current cycle parity-check bits?
		.is_last(encode_is_last),	//	Is a current cycle the last output cycle?
		.ready(encode_ready)		//	Is module ready for new encoding?
	);

// //	(2)
	//	Syndrome calculation module
	//	Set parameters in bch_syndrome module
	bch_syndrome sim_syndrome (
		.clk(clk),
		.start(syndrome_start),
		.enb(syndrome_enable),
		.data_in(decode_in),
		.expanded_syndromes(expanded_syndromes),
		.ready(syndrome_ready),
		.finish(syndrome_finish)
	);

//	(3)
	//	Key equation solving
	bch_key #(M, N, K, T, P) sim_key (
		.clk(clk),
		.start(syndrome_finish&&key_ready),
		.ready(key_ready),
		.expanded_syndromes(expanded_syndromes),	// Syndromes for t=1, 2, ..., 50
		.sigma(sigma),				//	Error location polynomial
		.finish(key_finish),		
		.ack_finish(chien_finish),	//	Start key equation solving as soon as Chien search is ready
		.l(elp_degree)				//	Degree of error location polynomial
	);

//	(4)
	//	Chien search
	bch_chien sim_chien (
		.clk(clk),
		.start(key_finish),
		.sigma(sigma),
		.elp_degree(elp_degree),
		.ready(chien_ready),
		.finish(chien_finish),
		.is_first(chien_is_first),
		.is_last(chien_is_last),
		.is_valid(chien_valid),
		.error(chien_error),
		.fail(chien_fail),
		.cntr(chien_cntr)
	);

	//	----------------------------------------------------------
	//		Signal manipulation
	//	----------------------------------------------------------

	always @(posedge clk) begin
//	(1)
		if (encode_accept_new) begin
			//	Store new input if new data is encoded
			encode_buffer <= #1 data_in << 16;
			//	Store error if new data is encoded
			error_buffer <= #1 error_in << 16;
		end else if (!encode_ready && encode_enable) begin
			//	Shift encoder buffer
			encode_buffer <= #1 encode_buffer << 16;
			//	Shfit error buffer
			error_buffer <= #1 error_buffer << 16;
		end

//	(4)
		if (chien_is_first) begin
			//	Initialize chien_error location buffer and store chien_error location
			chien_error_buffer <= #1 (chien_error[15:0]<<(31*16-1));
			temp1 <= #1 30;
		end else if (chien_valid && chien_is_last) begin
			chien_error_buffer <= #1 chien_error_buffer^temp2;
		end else if (chien_valid) begin
			chien_error_buffer <= #1 chien_error_buffer^temp2;
			temp1 <= #1 temp1-1;			
		end
		if (chien_finish) begin
			$fwrite(file_data,"%b\n", data_at_key);
			$fwrite(file_error,"%b\n", error_at_key);
			$fwrite(file_number,"%d\n", num_err_at_key);
			$fwrite(file_cycle,"%d\n", chien_cntr);
			if (chien_fail) begin
				//	If fails to correct errror
				$fwrite(file_result,"fail\n");
			end else begin
				if((chien_error_buffer^temp2)==error_at_chien) begin
					$fwrite(file_result,"correct\n");
				end else begin
					$fwrite(file_result,"wrong\n");
				end
			end
			$fflush();
		end

//	(X)
		//	When encoder/syndrome module start,
		if (encode_accept_new) begin
			//	Store data input to the next stage
			data_at_syndrome <= #1 data_in;
			//	Store error input to the next stage
			error_at_syndrome <= #1 error_in;
			num_err_at_syndrome <= #1 num_err;
		end
		//	When key solving equation module start,
		if (syndrome_finish&&key_ready) begin
			//	Store data input to the next stage
			data_at_key <= #1 data_at_syndrome;
			//	Store error input to the next stage
			error_at_key <= #1 error_at_syndrome;
			num_err_at_key <= #1 num_err_at_syndrome;
		end
		//	When Chien search start,
		if (key_finish) begin
			//	Store data input to the next stage
			data_at_chien <= #1 data_at_key;
			//	Store error input to the next stage
			error_at_chien <= #1 error_at_key;
			num_err_at_chien <= #1 num_err_at_key;
		end

	end

//	(X)
	// Export text file for comparision
	integer file_data;
	integer	file_error;
	integer	file_number;
	integer	file_cycle;
	integer	file_result;
	initial begin
		//	Open file for write
		file_data = $fopen("sim_data.out","w");
		file_error = $fopen("sim_error.out","w");
		file_number = $fopen("sim_number.out","w");
		file_cycle = $fopen("sim_cycle.out","w");
		file_result = $fopen("sim_result.out","w");
		// // For some condition/may be place in a seperate loop
		// $fclose(file);
	end

endmodule
/*
 * (511,304,25) BCH Decoder
 *
 * Copyright 2015: Distributed under 2-clause BSD license as contained in COPYING file.
 */

module bch_key #(
	parameter	M = 9,		//	Degree of field
	parameter	N = 511,	//	Code length
	parameter	K = 304,	//	Data length
	parameter	T = 25,		//	Number of correctable bits
	parameter	P = 207		//	Number of parity check bits
)(
	input	clk,
	input	start,
	input	[(2*T+1)*M-1:M]	expanded_syndromes,	// Syndromes values of 1,...,2t-1
	input	ack_finish,
	output	reg	finish = 0,
	output	ready,
	output	reg 	[(T+1)*M-1:0]	sigma = 0,	//	Error location polynomial / higher-degree coefficient at MSB / lower-degree coefficient at LSB / Size: (T+1)*M
	output	reg		[5:0]			l	= 0	//	Auxiliary degree / Size = current stage
);
	localparam	DONE = 2*T-1;

	reg		[2*T*M-1:0]		syndromes_reg;		//	Temporary register for storing expanded syndromes
	reg		[(T+1)*M-1:0]	syndrome_shift_reg;		//	Syndrome shift register used for computing discrepancy
	reg		[(T+1)*M-1:0]	beta;		//	Auxiliary polynomial / Size: (T+1)*M
	reg		[M-1:0]			delta;		//	Previous discrepancy_{k}
	
	wire	[M-1:0]			new_syndrome;
	wire	[(T+1)*M-1:0]	dk_terms;	//	Discrepancy terms_{k} computed from multiplication of sigma and syndrome
	wire	[M-1:0]			dk;	//	Discrepancy_{k} = summation of discrepancy terms

	wire	[(T+1)*M-1:0]		dk_x_beta;		//	Multiplication of discrepancy and auxiliary polynomial from the previous stage
	wire	[(T+1)*M-1:0]	delta_x_sigma;		//	Multiplication of delta and error location polnomial from the previous stage

	wire	[5:0]			k;	//	Current stage

	wire	cond;
	assign cond = (dk!=9'b0)&&(k>=(l<<1));

	reg		busy = 0;

	assign ready = !busy && (!finish || ack_finish);
	
	//	Counter module
	//	Size: log2(2*T+1) = log2(50+1) = 6
	counter #(6) syndrome_counter(
		.clk(clk),
		.enb(busy),
		.rst(start),
		.cntr(k)
	);

	//	Compute discrepancy (d_{k})
	genvar i;
	for (i=0; i<=T; i=i+1) begin : DKTERM
		finite_parallel_multiplier #(.LENGTH(M),.NUM_INPUT(1)) key_dkterms (
			.in1(syndrome_shift_reg[i*M+:M]),
			.in2(sigma[i*M+:M]),
			.out(dk_terms[i*M+:M])
		);
	end

	finite_parallel_adder #(.LENGTH(M),.NUM_INPUT(T+1)) key_dk (
		.in(dk_terms),
		.out(dk)
	);

	//	Compute dk x beta
	for (i=0; i<=T; i=i+1) begin : DKxBETA
		finite_parallel_multiplier #(.LENGTH(M),.NUM_INPUT(1)) key_dkterms (
			.in1(dk),
			.in2(beta[i*M+:M]),
			.out(dk_x_beta[i*M+:M])
		);
	end

	//	Compute delta x sigma
	for (i=0; i<=T; i=i+1) begin : DELTAxSIGMA
		finite_parallel_multiplier #(.LENGTH(M),.NUM_INPUT(1)) key_dkterms (
			.in1(delta),
			.in2(sigma[i*M+:M]),
			.out(delta_x_sigma[i*M+:M])
		);
	end

	always @(posedge clk) begin
		if (start) begin
			//	Initialize syndrome shift register
			syndromes_reg <= #1 (expanded_syndromes>>M);
			syndrome_shift_reg <= #1 { {(T*M){1'b0}},{expanded_syndromes[M+:M]} };
			//	Sigma^{0} = 1
			sigma <= #1 234'b1;		
			//	Beta^{0} = X
			beta <= #1 (225'b1<<M);
			//	l_{0} = 0
			l <= #1 6'b0;
			//	delta_{0} = 1
			delta <= #1 9'b1;

		end else if (busy) begin
			//	Shift syndromeds LFSR
			syndromes_reg <= #1 (syndromes_reg>>M);
			syndrome_shift_reg <= #1 { {syndrome_shift_reg[0+:(T*M)]},{syndromes_reg[0+:M]} };
			//	Compute	error location polynomial / sigma^{k+1} = delta_{k}*sigma^{k}(X) + d_{k}*beta^{k}(X)
			sigma <= #1 delta_x_sigma^dk_x_beta;

			if (cond) begin
				//	If d_k!=0 and k>=2l_{k}
				//	Store sigma value as a new auxiliary polynomial/ beta^{k+1} = sigma^{k}
				beta <= #1 { {sigma[T*M-1:0]},{M{1'b0}} };
				//	Compute new auxiliary degree
				l <= #1 k-l+1;
				//	Store new discrepancy
				delta <= #1 dk;
			end else begin
				//	Otherwise, 
				beta <= #1 { {beta[T*M-1:0]},{M{1'b0}} };
				l <= #1 l;
				delta <= #1 delta;
			end
		end

		if (start) begin
			finish <= #1 0;
			busy <= #1 1;
		end else if (busy && (k==DONE)) begin
			finish <= #1 1;
			busy <= #1 0;
		end else begin
			finish <= #1 0;
		end

	end

endmodule 